<?php

// /////////////////////////////////////////////////////////////////////////////
// WORKING AREA
// THIS IS AN AREA WHERE YOU SHOULD WRITE YOUR CODE AND MAKE CHANGES
// /////////////////////////////////////////////////////////////////////////////

namespace App;
/**
 * Class Square
 * Describes the base class for the width
 * @package App
 */
class Square extends GeometricShape Implements ShapeInterface, PolygonInterface 
{
	// Properties
	public $edgeSize;

	// Methods
	public function __construct($edgeSize) 
	{
		$this->edgeSize = $edgeSize;
	}

	/**
	 * Class Square
	 * Calculates Square Perimeter
	 * @package App
	 */
	public function getPerimeter(): float 
	{

		return (4 * $this->edgeSize);

	}

	/**
	 * Class Square
	 * Calculates Square Area
	 * @package App
	 */
	public function getArea(): float 
	{

		return pow($this->edgeSize, 2);
	}

	/**
	 * Class Square
	 * Gets Square number of angles
	 * @package App
	 */
	public function getAngles(): int 
	{
		return 4;
	}
}