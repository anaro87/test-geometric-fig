<?php

// /////////////////////////////////////////////////////////////////////////////
// WORKING AREA
// THIS IS AN AREA WHERE YOU SHOULD WRITE YOUR CODE AND MAKE CHANGES
// /////////////////////////////////////////////////////////////////////////////

namespace App;

/**
 * Class Circle
 * Describes the base class for the Circle
 * @package App
 */
class Circle extends GeometricShape Implements ShapeInterface 
{
	// Properties
	protected $radius;

	// Methods

	public function __construct($radius) 
	{
		$this->radius = $radius;
	}

	/**
	 * Class Circle
	 * Calculates Circle Perimeter
	 * @package App
	 */
	public function getPerimeter(): float 
	{

		return Circle::PI * 2 * $this->radius;

	}

	/**
	 * Class Circle
	 * Calculates Circle Area
	 * @package App
	 */
	public function getArea(): float 
	{

		return Circle::PI * pow($this->radius, 2);
	}
}