<?php

// /////////////////////////////////////////////////////////////////////////////
// WORKING AREA
// THIS IS AN AREA WHERE YOU SHOULD WRITE YOUR CODE AND MAKE CHANGES
// /////////////////////////////////////////////////////////////////////////////

namespace App;

/**
 * Class Rectangle
 * Describes the base class for the width
 * @package App
 */
class Rectangle extends GeometricShape Implements ShapeInterface, PolygonInterface 
{
	// Properties
	public $height;
	public $width;

	// Methods
	public function __construct($width, $height) 
	{
		$this->height = $height;
		$this->width = $width;
	}

	/**
	 * Class Rectangle
	 * Calculates Rectangle Perimeter
	 * @package App
	 */
	public function getPerimeter(): float 
	{

		return ($this->width * 2) + ($this->height * 2);

	}

	/**
	 * Class Rectangle
	 * Calculates Rectangle Area
	 * @package App
	 */
	public function getArea(): float 
	{

		return $this->width * $this->height;
	}

	/**
	 * Class Rectangle
	 * Gets Rectangle number of angles
	 * @package App
	 */
	public function getAngles(): int 
	{
		return 4;
	}
}